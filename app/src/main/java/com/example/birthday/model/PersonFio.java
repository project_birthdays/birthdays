package com.example.birthday.model;

import android.support.annotation.NonNull;

public class PersonFio {
    private long id;
    private String name, surname, patronymic;

    public PersonFio() {}

    public PersonFio(long idl, String name, String surname, String patronymic) {
        this.id = idl;
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    @Override
    @NonNull
    public String toString() {
        return (this.patronymic == null) ? this.name + " " + this.surname
                : this.surname + " " + this.name + " " + this.patronymic;
    }
}