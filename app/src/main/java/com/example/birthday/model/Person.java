package com.example.birthday.model;

public class Person {

    private PersonDate personDate;
    private PersonFio personFio;

    public Person(PersonDate personDate, PersonFio personFio) {
        this.personDate = personDate;
        this.personFio = personFio;
    }

    public PersonDate getPersonDate() {
        return personDate;
    }

    public PersonFio getPersonFio() {
        return personFio;
    }
}