package com.example.birthday.model;

import android.support.annotation.NonNull;

public class PersonDate {
    private long id;
    private int day, month;
    private Integer year;

    public PersonDate() {
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public PersonDate(long idl, int day, int month, Integer year) {
        this.id = idl;
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public long getId() {
        return id;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public Integer getYear() {
        return year;
    }

    @Override
    @NonNull
    public String toString() {
        return (this.year == null) ? getFormatValue(this.day) + "." + getFormatValue(this.month)
                : getFormatValue(this.day) + "." + getFormatValue(this.month) + "." + this.year;
    }

    private String getFormatValue(int value) {
        return (value < 10) ? "0" + value : String.valueOf(value);
    }
}