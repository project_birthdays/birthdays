package com.example.birthday;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.birthday.additionally.Keys;
import com.example.birthday.data.DateTable;
import com.example.birthday.data.FioTable;
import com.example.birthday.model.Person;
import com.example.personDate.R;

import java.util.Calendar;
import java.util.Objects;

public class FoundPersonActivity extends AppCompatActivity implements Keys {

    private long personId;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_found_person);

        TextView yearsOldHeader = findViewById(R.id.yearOldHeader);
        yearsOldHeader.setVisibility(View.INVISIBLE);
        TextView yearsOldView = findViewById(R.id.personYearOld);
        yearsOldHeader.setVisibility(View.INVISIBLE);

        TextView personNameView = findViewById(R.id.person_name);
        TextView personSurnameView = findViewById(R.id.person_surname);
        TextView personPatronymicView = findViewById(R.id.person_patronymic);
        TextView personDayView = findViewById(R.id.person_day);
        TextView personMonthView = findViewById(R.id.person_month);
        TextView personYearView = findViewById(R.id.person_year);

        FioTable fioTable = new FioTable();
        DateTable dateTable = new DateTable();
        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            personId = extras.getLong(KEY_PERSON_ID);
        }
        if (personId > 0) {
            Person person = new Person(dateTable.getPersonDateById(personId), fioTable.getPersonFioById(personId));

            personNameView.setText(person.getPersonFio().getName());
            personSurnameView.setText(person.getPersonFio().getSurname());
            if (person.getPersonFio().getPatronymic() == null) {
                TextView patronymicHeader = findViewById(R.id.patronymicHeader);
                patronymicHeader.setVisibility(View.GONE);
                personPatronymicView.setVisibility(View.GONE);
            } else {
                personPatronymicView.setText(person.getPersonFio().getPatronymic());
            }

            personDayView.setText(Integer.toString(person.getPersonDate().getDay()));
            personMonthView.setText(Integer.toString(person.getPersonDate().getMonth()));
            if (person.getPersonDate().getYear() == null) {
                TextView yearHeader = findViewById(R.id.yearHeader);
                yearHeader.setVisibility(View.GONE);
                personYearView.setVisibility(View.GONE);
            } else {
                personYearView.setText(Integer.toString(person.getPersonDate().getYear()));
                yearsOldHeader.setVisibility(View.VISIBLE);
                yearsOldView.setVisibility(View.VISIBLE);
                yearsOldView.setText(ageCounter(person));
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private String ageCounter(Person person) {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;

        int age = calendar.get(Calendar.YEAR) - person.getPersonDate().getYear();
        if (person.getPersonDate().getMonth() > month &&
                person.getPersonDate().getDay() > day){
            age--;
        }else if (month == person.getPersonDate().getMonth() &&
                (day < person.getPersonDate().getDay())){
            age--;
        }
        return (age > 0) ? String.valueOf(age) : month - person.getPersonDate().getMonth() + " мес.";
    }
}