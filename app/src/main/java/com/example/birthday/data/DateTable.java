package com.example.birthday.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.birthday.model.PersonDate;

import java.util.Objects;

public class DateTable {

    private static final String TABLE_BIRTHDAY = "birthdays";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_DAY = "day";
    private static final String COLUMN_MONTH = "month";
    private static final String COLUMN_YEAR = "year";

    private static final String SQL_CREATE_BIRTHDAY_TABLE =
            "CREATE TABLE " + TABLE_BIRTHDAY + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    COLUMN_DAY + " INTEGER NOT NULL, " +
                    COLUMN_MONTH + " INTEGER NOT NULL, " +
                    COLUMN_YEAR + " INTEGER );";

    private static final String SQL_DELETE_BIRTHDAY_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_BIRTHDAY;

    private DataBaseHandler dataBaseHandler;
    private SQLiteDatabase sqLiteDatabase;
    private ContentValues values;

    public DateTable() {
        this.dataBaseHandler = DataBaseHandler.getInstance();
    }

    static String getColumnId() {
        return COLUMN_ID;
    }

    static String getTableDate() {
        return TABLE_BIRTHDAY;
    }

    void create(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_BIRTHDAY_TABLE);
    }

    void drop(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_DELETE_BIRTHDAY_TABLE);
    }

    public void addDate(PersonDate personDate) {
        sqLiteDatabase = dataBaseHandler.getWritableDatabase();
        dataBaseHandler.getWritableDatabase();
        values = getDateValues(personDate);
        sqLiteDatabase.insert(TABLE_BIRTHDAY, null, values);
        dataBaseHandler.closeConnection();
    }

    public void updateDate(PersonDate personDate) {
        sqLiteDatabase = dataBaseHandler.getWritableDatabase();
        values = getDateValues(personDate);
        sqLiteDatabase.update(TABLE_BIRTHDAY, values, COLUMN_ID + " = ?",
                new String[]{String.valueOf(personDate.getId())});
        dataBaseHandler.closeConnection();
    }

    public PersonDate getPersonDateById(long personId) {
        sqLiteDatabase = dataBaseHandler.getReadableDatabase();
        try (Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_BIRTHDAY + " WHERE _id=? ", new String[]{String.valueOf(personId)})) {
            if (cursor != null) {
                cursor.moveToFirst();
            }
            return new PersonDate(Long.parseLong(Objects.requireNonNull(cursor).getString(0)), Integer.parseInt(cursor.getString(1)), Integer.parseInt(cursor.getString(2)), (cursor.getString(3) == null) ? null : Integer.parseInt(cursor.getString(3)));
        }
    }

    public void deletePerson(PersonDate personDate) {
        sqLiteDatabase = dataBaseHandler.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_BIRTHDAY, COLUMN_ID + " = ?", new String[] { String.valueOf(personDate.getId())});
        dataBaseHandler.closeConnection();
    }

    private ContentValues getDateValues(PersonDate personDate){
        ContentValues values = new ContentValues();
        values.put(COLUMN_DAY, personDate.getDay());
        values.put(COLUMN_MONTH, personDate.getMonth());
        values.put(COLUMN_YEAR, personDate.getYear());
        return values;
    }
}