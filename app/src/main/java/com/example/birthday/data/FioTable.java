package com.example.birthday.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.birthday.model.PersonFio;

import java.util.Objects;

public class FioTable {

    private static final String TABLE_PERSON = "persons";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_SURNAME = "surname";
    private static final String COLUMN_PATRONYMIC = "patronymic";

    private static final String SQL_CREATE_PERSON_TABLE =
            "CREATE TABLE " + TABLE_PERSON + " (" +
                    COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                    COLUMN_NAME + " TEXT NOT NULL," +
                    COLUMN_SURNAME + " TEXT NOT NULL, " +
                    COLUMN_PATRONYMIC + " TEXT );";

    private static final String SQL_DELETE_PERSON_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_PERSON;

    private DataBaseHandler dataBaseHandler;
    private SQLiteDatabase sqLiteDatabase;
    private ContentValues values;

    public FioTable() {
        this.dataBaseHandler = DataBaseHandler.getInstance();
    }

    static String getTableFio() {
        return TABLE_PERSON;
    }

    static String getColumnId() {
        return COLUMN_ID;
    }

    void create(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_PERSON_TABLE);
    }

    void drop(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_DELETE_PERSON_TABLE);
    }

    public void addPerson(PersonFio personFio) {
        sqLiteDatabase = dataBaseHandler.getWritableDatabase();
        dataBaseHandler.getWritableDatabase();
        values = getFioValues(personFio);
        sqLiteDatabase.insert(TABLE_PERSON, null, values);
        dataBaseHandler.closeConnection();
    }

    public void updatePerson(PersonFio personFio) {
        sqLiteDatabase = dataBaseHandler.getWritableDatabase();
        values = getFioValues(personFio);
        sqLiteDatabase.update(TABLE_PERSON, values, COLUMN_ID + " = ?",
                new String[]{String.valueOf(personFio.getId())});
        dataBaseHandler.closeConnection();
    }

    public PersonFio getPersonFioById(long personId) {
        sqLiteDatabase = dataBaseHandler.getReadableDatabase();
        try (Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_PERSON + " WHERE _id=? ", new String[]{String.valueOf(personId)})) {
            if (cursor != null) {
                cursor.moveToFirst();
            }
            return new PersonFio(Long.parseLong(Objects.requireNonNull(cursor).getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3));
        }
    }

    public void deletePerson(PersonFio personFio) {
        sqLiteDatabase = dataBaseHandler.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_PERSON, COLUMN_ID + " = ?", new String[] { String.valueOf(personFio.getId())});
        dataBaseHandler.closeConnection();
    }

    private ContentValues getFioValues(PersonFio personFio){
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME, personFio.getName());
        values.put(COLUMN_SURNAME, personFio.getSurname());
        values.put(COLUMN_PATRONYMIC, personFio.getPatronymic());
        return values;
    }
}