package com.example.birthday.data;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.birthday.model.Person;
import com.example.birthday.model.PersonDate;
import com.example.birthday.model.PersonFio;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DataBaseHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "PersonData.database";
    private static final int DATABASE_VERSION = 1;

    private FioTable fioTable;
    private DateTable dateTable;
    private SQLiteDatabase sqLiteDatabase;

    private static DataBaseHandler instance;

    public static DataBaseHandler getInstance() {
        return instance;
    }

    public static void setInstance(Context context) {
        instance = new DataBaseHandler(context);
    }

    private DataBaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        fioTable = new FioTable();
        dateTable = new DateTable();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        fioTable.create(db);
        dateTable.create(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        fioTable.drop(db);
        dateTable.drop(db);
        fioTable.create(db);
        dateTable.create(db);
    }

    public List<Person> getAllPersonsList() {
        List<Person> personsList = new ArrayList<>();
        sqLiteDatabase = this.getReadableDatabase();
        try (Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + DateTable.getTableDate() +" INNER JOIN " + FioTable.getTableFio()+"  ON "
                + DateTable.getTableDate() + "." + DateTable.getColumnId() + "= " + FioTable.getTableFio()+ "." + FioTable.getColumnId()+";", null)) {
            if (cursor.moveToFirst()) {
                do {
                    Person person = new Person(initPersonDate(cursor), initPersonFio(cursor));
                    personsList.add(person);
                } while (cursor.moveToNext());
            }
            return personsList;
        }
    }

    public List<Person> getDataByKeyword(String query) {
        List<Person> personsList = new ArrayList<>();
        sqLiteDatabase = this.getReadableDatabase();
        try (Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + DateTable.getTableDate() +" INNER JOIN " + FioTable.getTableFio()+"  ON "
                + DateTable.getTableDate() + "." + DateTable.getColumnId() + "= " + FioTable.getTableFio()+ "." + FioTable.getColumnId() + " " +
                "WHERE name LIKE ? or surname LIKE ? or patronymic LIKE ? " +
                "or day LIKE ? or month LIKE ? or year LIKE ?", new String[]{query+"%", query+"%", query+"%", query+"%", query+"%", query+"%"})) {
            if (cursor.moveToFirst()) {
                do {
                    Person person = new Person(initPersonDate(cursor), initPersonFio(cursor));
                    personsList.add(person);
                } while (cursor.moveToNext());
            }
            return personsList;
        }
    }

    public void closeConnection() {
        sqLiteDatabase = this.getReadableDatabase();
        sqLiteDatabase.close();
    }

    private PersonDate initPersonDate(Cursor cursor) {
        PersonDate personDate = new PersonDate();
        personDate.setId(Long.parseLong(Objects.requireNonNull(cursor).getString(0)));
        personDate.setDay(Integer.parseInt(cursor.getString(1)));
        personDate.setMonth(Integer.parseInt(cursor.getString(2)));
        personDate.setYear((cursor.getString(3) == null) ? null : Integer.parseInt(cursor.getString(3)));
        return personDate;
    }

    private PersonFio initPersonFio(Cursor cursor) {
        PersonFio personFio = new PersonFio();
        personFio.setId(Long.parseLong(Objects.requireNonNull(cursor).getString(4)));
        personFio.setName(cursor.getString(5));
        personFio.setSurname(cursor.getString(6));
        personFio.setPatronymic(cursor.getString(7));
        return personFio;
    }
}