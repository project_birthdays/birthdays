package com.example.birthday.errors;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.widget.EditText;

import com.example.personDate.R;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ErrorChecking {
    private Resources resources;

    public ErrorChecking(Context context){
        this.resources = context.getResources();
    }

    public boolean inputFieldIsEmpty(EditText editText, String error){
        if(TextUtils.isEmpty(editText.getText())){
            editText.setError(error);
            return true;
        }
        return false;
    }



    public boolean misspellingPerson(EditText editText, String error){
        if (!editText.getText().toString().isEmpty()) {
            String name = editText.getText().toString();
            Pattern pattern = Pattern.compile("^([A-ZА-Яa-zа-я]+-)*([A-ZА-Яa-zа-я ]+)$");
            Matcher matcher = pattern.matcher(name);
            if (!matcher.find()) {
                editText.setError(error);
                return true;
            }
        }
        return false;
    }

    public boolean misspellingDate(EditText editText){
        if (!editText.getText().toString().isEmpty()) {
            int day = Integer.parseInt(editText.getText().toString());
            if (day > 31 || day <= 0) {
                editText.setError(resources.getString(R.string.error_date));
                return true;
            }
        }
        return false;
    }
    public boolean misspellingMonth(EditText editText){
        if (!editText.getText().toString().isEmpty()) {
            int month = Integer.parseInt(editText.getText().toString());
            if (month > 12 || month <= 0) {
                editText.setError(resources.getString(R.string.error_month));
                return true;
            }
        }
        return false;
    }


    public boolean misspellingYear(EditText editText){
        Calendar calendar = Calendar.getInstance();
        if (!editText.getText().toString().isEmpty()) {
            if (1900 > Integer.parseInt(editText.getText().toString()) | Integer.parseInt(editText.getText().toString()) > calendar.get(Calendar.YEAR)) {
                editText.setError(resources.getString(R.string.error_year));
                return true;
            }
        }
        return false;
    }
}