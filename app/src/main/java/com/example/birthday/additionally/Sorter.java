package com.example.birthday.additionally;

import com.example.birthday.model.Person;

import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Sorter {

    private List<Person> personList;

    public Sorter(List<Person> personList) {
        this.personList = personList;
    }

    public List<Person> sortingByYear() {
        Collections.sort(personList, new Comparator<Person>() {
            @Override
            public int compare(Person person1, Person person2) {
                return getDateByOrder(person1).compareTo(getDateByOrder(person2));
            }});
        return personList;
    }

    public List<Person>  sortingByNearestBirthday() {
        Collections.sort(personList, new Comparator<Person>() {
            @Override
            public int compare(Person person1, Person person2) {
                return getTimeByCurrentDate(person1).compareTo(getTimeByCurrentDate(person2));
            }
        });
        return personList;
    }

    private Date getDateByOrder(Person person) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(0, person.getPersonDate().getMonth(), person.getPersonDate().getDay());
        return calendar.getTime();
    }

    private Date getTimeByCurrentDate(Person person) {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        if (person.getPersonDate().getMonth() > month){
            calendar.set(0, person.getPersonDate().getMonth() - month, person.getPersonDate().getDay() - day);
        } else if (person.getPersonDate().getMonth() == month && person.getPersonDate().getDay() > day) {
            calendar.set(0, person.getPersonDate().getMonth() - month, person.getPersonDate().getDay() - day);
        }
        else calendar.set(1, person.getPersonDate().getMonth(), person.getPersonDate().getDay());
        return calendar.getTime();
    }
}