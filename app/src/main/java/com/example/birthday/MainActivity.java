package com.example.birthday;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.SearchView;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import com.example.birthday.additionally.Keys;
import com.example.birthday.additionally.Sorter;
import com.example.birthday.data.DataBaseHandler;
import com.example.birthday.model.Person;
import com.example.birthday.recycler.OnPersonClickListener;
import com.example.birthday.recycler.PersonAdapter;
import com.example.personDate.R;

import java.util.List;

public class MainActivity extends AppCompatActivity implements Keys {

    private TextView sortTextView;
    private DataBaseHandler dataBaseHandler;
    private SearchView searchView;

    private RecyclerView personsListRecyclerView;
    private OnPersonClickListener personClickListener;
    private List<Person> personList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sortTextView = findViewById(R.id.sort);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DataBaseHandler.setInstance(this);

        dataBaseHandler = DataBaseHandler.getInstance();
        personsListRecyclerView = findViewById(R.id.recyclerPersonList);
        personsListRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        personList = dataBaseHandler.getAllPersonsList();
        personClickListener = new OnPersonClickListener() {
            @Override
            public void onItemClick(Person person, View v) {
                startPersonActivity(person);
            }
        };
        initializationPersonList(personList, personClickListener);
        runAnimation();

        FloatingActionButton addPerson = findViewById(R.id.fab);
        addPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPersonActivity(null);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        sortTextView.setText(R.string.sort_by_default_text);
        if (searchView != null && !searchView.isIconified()) {
            searchView.setQuery("", false);
            searchView.clearFocus();
            searchView.onActionViewCollapsed();
            searchView.setIconified(true);
        }
        personList = dataBaseHandler.getAllPersonsList();
        initializationPersonList(personList, personClickListener);
        runAnimation();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dataBaseHandler.closeConnection();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem menuItem = menu.findItem(R.id.search);
        searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                if (TextUtils.isEmpty(newText)) {
                    sortTextView.setText(getString(R.string.sort_by_default_text));
                    personList = dataBaseHandler.getAllPersonsList();
                    personClickListener = new OnPersonClickListener() {
                        @Override
                        public void onItemClick(Person person, View v) {
                            startPersonActivity(person);
                        }
                    };
                } else {
                    sortTextView.setText(getString(R.string.search_text));
                    personList = dataBaseHandler.getDataByKeyword(newText);
                    personClickListener = new OnPersonClickListener() {
                        @Override
                        public void onItemClick(Person person, View v) {
                            startFoundActivity(person);
                        }
                    };
                }
                initializationPersonList(personList, personClickListener);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Sorter sorter = new Sorter(dataBaseHandler.getAllPersonsList());
        if (id == R.id.sort_by_default) {
            sortTextView.setText(getString(R.string.sort_by_default_text));
            initializationPersonList(dataBaseHandler.getAllPersonsList(), personClickListener);
            return true;
        }
        if (id == R.id.sort_by_order) {
            sortTextView.setText(getString(R.string.sort_by_order_text));
            initializationPersonList(sorter.sortingByYear(), personClickListener);
            return true;
        }
        if (id == R.id.sort_by_nearest_birthday) {
            sortTextView.setText(getString(R.string.sort_by_nearest_birthday));
            initializationPersonList(sorter.sortingByNearestBirthday(), personClickListener);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startPersonActivity(Person person) {
        Intent intent = new Intent(getApplicationContext(), PersonActivity.class);
        if (person != null) {
            intent.putExtra(KEY_PERSON_ID, person.getPersonFio().getId());
        } else intent.putExtra(KEY_PERSON_ID, 0);
        startActivity(intent);
    }

    private void startFoundActivity(Person person) {
        Intent intent = new Intent(getApplicationContext(), FoundPersonActivity.class);
        intent.putExtra(KEY_PERSON_ID, person.getPersonFio().getId());
        startActivity(intent);
    }

    private void initializationPersonList(List<Person> personList, OnPersonClickListener personClickListener) {
        PersonAdapter personAdapter = new PersonAdapter(personList, personClickListener);
        personsListRecyclerView.setAdapter(personAdapter);
    }

    private void runAnimation() {
        LayoutAnimationController controller;
        controller = AnimationUtils.loadLayoutAnimation(this, R.anim.layout_anim_recyclerview);
        personsListRecyclerView.setLayoutAnimation(controller);
        personsListRecyclerView.getAdapter().notifyDataSetChanged();
        personsListRecyclerView.scheduleLayoutAnimation();
    }
}