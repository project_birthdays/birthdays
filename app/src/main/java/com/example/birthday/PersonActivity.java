package com.example.birthday;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.example.birthday.errors.ErrorChecking;
import com.example.birthday.additionally.Keys;
import com.example.birthday.data.DateTable;
import com.example.birthday.data.DataBaseHandler;
import com.example.birthday.data.FioTable;
import com.example.birthday.model.Person;
import com.example.birthday.model.PersonDate;
import com.example.birthday.model.PersonFio;
import com.example.personDate.R;

import java.util.Objects;

public class PersonActivity extends AppCompatActivity implements Keys {

    private EditText nameEdit, surnameEdit, patronymicEdit, dayEdit, monthEdit, yearEdit;
    private FioTable fioTable;
    private DateTable dateTable;

    private long personId = 0;
    private Person person;

    private DataBaseHandler dataBaseHandler;

    @SuppressLint({"SetTextI18n", "RestrictedApi"})
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_person);
        FloatingActionButton delButton = findViewById(R.id.delete_button);

        nameEdit = findViewById(R.id.name);
        surnameEdit = findViewById(R.id.surname);
        patronymicEdit = findViewById(R.id.patronymic);
        dayEdit = findViewById(R.id.day_birthday);
        monthEdit = findViewById(R.id.month_birthday);
        yearEdit = findViewById(R.id.year_birthday);

        dataBaseHandler = DataBaseHandler.getInstance();
        fioTable = new FioTable();
        dateTable = new DateTable();
        Bundle extras = getIntent().getExtras();

        if (extras != null){
            personId = extras.getLong(KEY_PERSON_ID);
        }
        if (personId > 0) {
            person = new Person(dateTable.getPersonDateById(personId), fioTable.getPersonFioById(personId));
            nameEdit.setText(person.getPersonFio().getName());
            surnameEdit.setText(person.getPersonFio().getSurname());
            patronymicEdit.setText(person.getPersonFio().getPatronymic());
            dayEdit.setText(Integer.toString(person.getPersonDate().getDay()));
            monthEdit.setText(Integer.toString(person.getPersonDate().getMonth()));
            if (person.getPersonDate().getYear() == null) {
                yearEdit.setText("");
            } else yearEdit.setText(Integer.toString(person.getPersonDate().getYear()));
        } else {
            delButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickSavePerson(View view){
        ErrorChecking errorChecking = new ErrorChecking(this);
        if (errorChecking.inputFieldIsEmpty(nameEdit, getString(R.string.no_name)) | errorChecking.inputFieldIsEmpty(surnameEdit, getString(R.string.no_surname))
                | errorChecking.inputFieldIsEmpty(dayEdit, getString(R.string.no_date)) | errorChecking.inputFieldIsEmpty(monthEdit, getString(R.string.no_month))
                | errorChecking.misspellingPerson(nameEdit, getString(R.string.error_name)) | errorChecking.misspellingPerson(surnameEdit, getString(R.string.error_surname))
                | errorChecking.misspellingPerson(patronymicEdit, getString(R.string.error_patronymic))
                | errorChecking.misspellingDate(dayEdit) | errorChecking.misspellingMonth(monthEdit) | errorChecking.misspellingYear(yearEdit)) {
            return;
        }
        PersonFio personFio = initPersonFio();
        PersonDate personDate = initPersonDate();

        person = new Person(personDate, personFio);

        if (person.getPersonFio().getId() > 0) {
            dateTable.updateDate(person.getPersonDate());
            fioTable.updatePerson(person.getPersonFio());
        }else {
            dateTable.addDate(person.getPersonDate());
          fioTable.addPerson(person.getPersonFio());
       }
        goHome();
    }

    public void onClickDeletePerson(View view){
        dateTable.deletePerson(person.getPersonDate());
        fioTable.deletePerson(person.getPersonFio());
        goHome();
    }

    private PersonFio initPersonFio() {
        return new PersonFio(personId, nameEdit.getText().toString().trim(), surnameEdit.getText().toString().trim(),
                (TextUtils.isEmpty(patronymicEdit.getText())) ? null : patronymicEdit.getText().toString().trim());
    }

    private PersonDate initPersonDate(){
       return new PersonDate(personId, Integer.parseInt(dayEdit.getText().toString()),
                Integer.parseInt(monthEdit.getText().toString()), (TextUtils.isEmpty(yearEdit.getText())) ? null : Integer.parseInt(yearEdit.getText().toString()));
    }

    private void goHome() {
        dataBaseHandler.closeConnection();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }
}