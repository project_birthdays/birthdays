package com.example.birthday.recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.birthday.model.Person;
import com.example.personDate.R;

import java.util.List;

public class PersonAdapter extends RecyclerView.Adapter<PersonViewHolder> {

    private List<Person> personList;
    private OnPersonClickListener personClickListener;

    public PersonAdapter(List<Person> personList, OnPersonClickListener personClickListener) {
        this.personList = personList;
        this.personClickListener = personClickListener;
    }

    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_view_custom, viewGroup, false);
        return new PersonViewHolder(view, personClickListener, personList);
    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder personViewHolder, int position) {
        personViewHolder.getDate().setText(personList.get(position).getPersonDate().toString());
        personViewHolder.getFio().setText(personList.get(position).getPersonFio().toString());
    }

    @Override
    public int getItemCount() {
        return personList.size();
    }
}