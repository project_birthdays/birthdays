package com.example.birthday.recycler;

import android.view.View;

import com.example.birthday.model.Person;

public interface OnPersonClickListener {
    void onItemClick(Person person, View v);
}