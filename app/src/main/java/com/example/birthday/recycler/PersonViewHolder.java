package com.example.birthday.recycler;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.birthday.model.Person;
import com.example.personDate.R;

import java.util.List;

class PersonViewHolder extends RecyclerView.ViewHolder {

    private TextView fio;
    private TextView date;

    TextView getFio() {
        return fio;
    }

    TextView getDate() {
        return date;
    }

    @SuppressLint("ResourceType")
    PersonViewHolder(@NonNull final View itemView, @NonNull final OnPersonClickListener personClickListener, final List<Person> personList) {
        super(itemView);
        fio = itemView.findViewById(R.id.fio);
        date = itemView.findViewById(R.id.date);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                personClickListener.onItemClick(personList.get(getLayoutPosition()), v);
            }
        });
    }
}